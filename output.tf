output "public_ip_lab1" {
  value       = digitalocean_droplet.lab.ipv4_address
  description = "Public IP for lab1"
}

output "public_ip_lab2" {
  value       = digitalocean_droplet.lab2.ipv4_address
  description = "Public IP for lab2"
}
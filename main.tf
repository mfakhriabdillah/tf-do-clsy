# Create a new Web Droplet in the sgp1 region
resource "digitalocean_droplet" "lab" {
  image  = "ubuntu-20-04-x64"
  name   = "lab-hacker-1"
  region = "sgp1"
  size   = "s-1vcpu-1gb"
}

resource "digitalocean_droplet" "lab2" {
  image  = "ubuntu-20-04-x64"
  name   = "lab-hacker-2"
  region = "sgp1"
  size   = "s-1vcpu-1gb"
}
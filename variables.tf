variable "do_token" {
  type        = string
  default = "[INSERT YOUR TOKEN HERE]"
  description = "Digital Ocean Token for Terraform"
}